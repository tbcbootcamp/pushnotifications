package com.example.pushnotifications

import android.app.Application
import android.content.Context
import com.onesignal.OneSignal

class App : Application() {

    companion object {
        lateinit var instance: App
        var context: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        initOneSignal()
    }

    private fun initOneSignal() {
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}